﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace AMcom.Teste.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ubs")]
    public class UbsController : ControllerBase
    {
        // Implemente um método que seja acessível por HTTP GET e retorne a lista das 5 UBSs
        // (Unidades Básicas de Saúde) mas próximas de um ponto (latitude e longitude) e ordenada
        // por avaliação (da melhor para a pior). O retorno deve ser no formato JSON.

        private Service.IService<Service.UbsDTO> service;
        public UbsController(Service.IService<Service.UbsDTO> service)
        {
            this.service = service;

            var pathFile = $"{Environment.CurrentDirectory}\\ubs.csv";

            service.Load(pathFile);
        }

        [HttpGet]
        public IActionResult GetNearUbs(double latitude, double longitude)
        {
            var itens = service.Filter(latitude, longitude);
            return Ok(itens);
        }
    }
}