﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AMcom.Teste.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddTransient<DAL.IRepository<DAL.Ubs>, DAL.UbsRepository>();
            services.AddTransient<Service.IService<Service.UbsDTO>, Service.UbsService>();

            /*
            services.AddSingleton<Service.IService<Service.UbsDTO>>(provider =>
            {
                var service = new Service.UbsService(provider.GetService<DAL.IRepository<DAL.Ubs>>());
                service.Load("D:\\Documents\\Projetos\\WEB\\TESTE-AMCOM\\dotnet-master\\dotnet-master\\src\\AMcom.Teste.DAL\\ubs.csv");
                return service;
            });
            */
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
