﻿using GeoCoordinatePortable;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace AMcom.Teste.Service
{
    public class UbsService : IService<UbsDTO>
    {
        // Implemente um método que retorne as 5 UBSs mais próximas de um ponto (latitude e longitude) que devem 
        // ser passados como parâmetro para o método e retorne uma lista/coleção de objetos do tipo UbsDTO.
        // Esta lista deve estar ordenada pela avaliação (da melhor para a pior) de acordo com os dados que constam
        // no objeto retornado pela camada de acesso a dados (DAL).

        List<DAL.Ubs> elements;
        private DAL.IRepository<DAL.Ubs> repository;

        public UbsService(DAL.IRepository<DAL.Ubs> _repository)
        {
            repository = _repository;
        }

        public void Load(string filePath)
        {
            elements = repository.LoadElements(filePath);
        }

        public List<UbsDTO> Filter(double latitude, double longitude)
        {
            var localFilter = new GeoCoordinate(latitude, longitude);

            var elementsFiltered = elements.Select(element =>
            {
                var distance = localFilter.GetDistanceTo(new GeoCoordinate(element.vlr_latitude, element.vlr_longitude));
                return new UbsFilter(element, distance);
            });

            var elementsOrderedByDistance = elementsFiltered.OrderBy(element => element.DistanceOf).Take(5).ToList();
            var elementsOrderedByClassificacao = elementsOrderedByDistance.OrderByDescending((element => element.Element.Classificacao)).ThenBy(x => x.Element.nome_estab);

            return elementsOrderedByClassificacao
                .Select(element => BuildDTO(element.Element))
                .ToList();
        }

        private UbsDTO BuildDTO(DAL.Ubs element)
        {
            return new UbsDTO(element.nome_estab, $"{element.dsc_endereco}, {element.dsc_bairro}, {element.dsc_cidade}", element.dsc_estruct_fisic_ambiencia);
        }
    }
}
