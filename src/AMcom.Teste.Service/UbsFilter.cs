﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMcom.Teste.Service
{
    internal class UbsFilter
    {
        public UbsFilter(DAL.Ubs _element, double _distance)
        {
            Element = _element;
            DistanceOf = _distance;
        }

        public DAL.Ubs Element { get; set; }
        public double DistanceOf { get; set; }
    }
}
