﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMcom.Teste.Service
{
    public interface IService<T>
    {
        void Load(string filePath);
        List<T> Filter(double latitude, double longitute);
    }
}
