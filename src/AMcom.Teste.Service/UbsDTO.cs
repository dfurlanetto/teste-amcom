﻿using GeoCoordinatePortable;
using System.Collections.Generic;

namespace AMcom.Teste.Service
{
    public class UbsDTO
    {
        public UbsDTO(string nome, string endereco, string avaliacao)
        {
            Nome = nome;
            Endereco = endereco;
            Avaliacao = avaliacao;
        }

        public string Nome { get; private set; }
        public string Endereco { get; private set; }
        public string Avaliacao { get; private set; }

        public bool Equals(UbsDTO item)
        {
            bool ret = true;

            if (item.Nome != this.Nome)
                ret = false;
            if (item.Endereco != this.Endereco)
                ret = false;
            if (item.Avaliacao != this.Avaliacao)
                ret = false;
            
            return ret;
        }
    }
}
