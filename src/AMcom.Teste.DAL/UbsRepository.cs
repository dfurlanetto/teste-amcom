﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AMcom.Teste.DAL
{

    public class UbsRepository : IRepository<Ubs>
    {
        // Implemente um método que retorne uma lista/coleção de objetos do tipo Ubs.
        // Caso necessário, crie um parâmetro para filtrar os objetos dessa coleção se a lógica não for 
        // implementada na camada de serviços.

        private List<Ubs> itens;

        public List<Ubs> LoadElements(string filePath)
        {
            itens = new List<Ubs>();

            var lines = File.ReadAllLines(filePath);

            foreach (var line in lines.Skip(1))
            {
                var values = line.Split(',');

                try
                {
                    var item = new Ubs(
                        Convert.ToDouble(values[0].Replace('.', ',')),
                        Convert.ToDouble(values[1].Replace('.', ',')),
                        values[2],
                        values[3],
                        values[4],
                        values[5],
                        values[6]);

                    itens.Add(item);
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed to load a item of ubs.", ex);
                }

            }

            var distintc = itens.Select(s => s.dsc_estruct_fisic_ambiencia).Distinct();

            return itens;
        }
    }
}