﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMcom.Teste.DAL
{
    public interface IRepository<T>
    {
        List<T> LoadElements(string filePath);
    }
}
