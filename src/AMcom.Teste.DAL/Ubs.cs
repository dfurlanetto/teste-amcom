﻿using System.Collections.Generic;
using System.IO;

namespace AMcom.Teste.DAL
{
    public class Ubs
    {
        // Esta classe deve conter as seguintes propriedades:
        // vlr_latitude, vlr_longitude, nom_estab, dsc_endereco, dsc_bairro, dsc_cidade, dsc_estrut_fisic_ambiencia

        public Ubs(double vlr_latitude, double vlr_longitude, string nome_estab, string dsc_endereco, string dsc_bairro, string dsc_cidade, string dsc_estruct_fisic_ambiencia)
        {
            this.vlr_latitude = vlr_latitude;
            this.vlr_longitude = vlr_longitude;
            this.nome_estab = nome_estab;
            this.dsc_endereco = dsc_endereco;
            this.dsc_bairro = dsc_bairro;
            this.dsc_cidade = dsc_cidade;
            this.dsc_estruct_fisic_ambiencia = dsc_estruct_fisic_ambiencia;
            this.Classificacao = ClassificacaoUbs.GetClassificacao(dsc_estruct_fisic_ambiencia.Replace(" ",""));
        }
        

        public double vlr_latitude { get; private set; }
        public double vlr_longitude { get; private set; }
        public string nome_estab { get; private set; }
        public string dsc_endereco { get; private set; }
        public string dsc_bairro { get; private set; }
        public string dsc_cidade { get; private set; }
        public string dsc_estruct_fisic_ambiencia { get; private set; }
        public int Classificacao { get; private set; }

    }

    public static class ClassificacaoUbs
    {
        private static Dictionary<string, int> DicionarioClassificacao = new Dictionary<string, int>
        {
            ["Desempenhomedianoouumpoucoabaixodamédia"] = 0,
            ["Desempenhoacimadamédia"] = 1,
            ["Desempenhomuitoacimadamédia"] = 2
        };

        public static int GetClassificacao(string descricao)
        {
            return DicionarioClassificacao[descricao];
        }
    }
}
