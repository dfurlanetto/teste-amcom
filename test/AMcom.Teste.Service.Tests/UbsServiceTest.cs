﻿using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace AMcom.Teste.Service.Tests
{
    [TestFixture]
    public class UbsServiceTest
    {
        // Implemente os testes unitários para o método criado no UbsService. Faça quantos testes achar
        // pertinente para validar a sua lógica de aplicação.

        Mock<DAL.IRepository<DAL.Ubs>> repository;
        Service.IService<Service.UbsDTO> service;
        string filePath;
        string[] fileResult;

        public UbsServiceTest()
        {
            filePath = Environment.CurrentDirectory;
        }

        [SetUp]
        public void Setup()
        {
            repository = new Mock<DAL.IRepository<DAL.Ubs>>();
            repository.Setup(x => x.LoadElements("")).Returns(new List<DAL.Ubs>()
            {
                new DAL.Ubs(-26.8537724018089,-49.130215644835,"ESF TEREZA LESCOVITZ","RUA FREDERICO JENSEN","ITOUPAVAZINHA","Blumenau","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-26.8709385395042,-49.0506720542893,"ESF GUSTAVO TRIBESS I","RUA HERMANN TRIBESS","FORTALEZA","Blumenau","Desempenho muito acima da média"),
                new DAL.Ubs(-26.8657779693596,-49.1213536262498,"ESF GERALDO SCHMIDT SOBRINHO II","RUA JOHANN SACHSE","SALTO DO NORTE","Blumenau","Desempenho acima da média"),
                new DAL.Ubs(-26.8809592723839,-49.0550494194016,"ESF HASSO ROLF MULLER II","RUA ROBERTO KOCK","FORTALEZA","Blumenau","Desempenho acima da média"),
                new DAL.Ubs(-26.8773007392875,-49.0585255622849,"ESF HASSO ROLF MULLER I","RUA HERMANN TRIBESS","FORTALEZA","Blumenau","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-26.8529677391044,-49.1202163696275,"ESF ARMANDO ODEBRECHT I","RUA PROF JACOB INEICHEN","ITOUPAVAZINHA","Blumenau","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-26.8657779693596,-49.1213536262498,"ESF GERALDO SCHMIDT SOBRINHO I","RUA JOHANN SACHSE","SALTO DO NORTE","Blumenau","Desempenho acima da média"),
                new DAL.Ubs(-10.6834959983823,-36.9425153732289,"UNIDADE DE SAUDE DA FAMILIA GENTIL ACIOLE GOMES","POVOADO AGUADA","AGUADA","Carmópolis","Desempenho acima da média"),
                new DAL.Ubs(-26.8709385395042,-49.0506720542893,"ESF GUSTAVO TRIBES II","RUA HERMANN TRIBESS","TRIBESS","Blumenau","Desempenho muito acima da média"),
                new DAL.Ubs(-22.748125791549,-50.5790162086472,"PSF VILA DOS PASSAROS TARUMA","AVENIDA ARAPONGAS","JARDIM DOS PASSAROS","Tarumã","Desempenho acima da média"),
                new DAL.Ubs(-26.8529677391044,-49.1202163696275,"ESF ARMANDO ODEBRECHT II","RUA PROF JACOB INEICHEN","ITOUPAVAZINHA","Blumenau","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-6.37739181518536,-42.8457784652698,"PS LAGOA","POVOADO LAGOA","ZONA RURAL","Amarante","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-9.93603944778413,-63.0161404609662,"CENTRO DE SAUDE SETOR 10 ARIQUEMES","RUA MEXICO","SETOR 10","Ariquemes","Desempenho acima da média"),
                new DAL.Ubs(-22.2237646579736,-49.6529889106736,"USF ARACELI","RUA SAO FRANCISCO DE ASSIS","ARACELI","Garça","Desempenho muito acima da média"),
                new DAL.Ubs(-13.7738513946529,-39.2989969253529,"PS DO KM 25","ROD ITUBERA GANDU","ROD ITUBERA GANDU","Ituberá","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-6.8560266494749,-36.4115023612965,"UBSF SERIDO","RUA JOSE BARBOSA DE MEDEIROS","CENTRO","Seridó","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-16.1997592449184,-49.6048164367661,"CENTRO DE SAUDEUNIDADE BASICA CENTRAL ITAUCU","RUA PORTO ALEGRE","CENTRO","Itauçu","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-15.9228372573848,-49.7905111312852,"PSF SAO BENEDITO","POVOADO DE SAO BENEDITO","SAO BENEDITO","Itaberaí","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-27.0745182037346,-52.637214660643,"CENTRO DE SAUDE DA FAMILIA CRISTO REI","RUA IMBITUBA","CRISTO REI","Chapecó","Desempenho muito acima da média"),
                new DAL.Ubs(-23.6302399635308,-46.443586349486,"UBS JARDIM SANTO ANDRE","MIGUEL FERREIRA DE MELO R","JARDIM SANTO ANDRE","São Paulo","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-10.5659508705136,-36.8158721923817,"UNIDADE DE SAUDE DA FAMILIA BADAJOS","POVOADO BADAJOS","RURAL","Japaratuba","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-28.2813942432395,-49.151823520659,"PROGRAMA SAUDE DA FAMILIA PSF FLORESTA","RUA WALDEMAR JOAO PAULINO","FLORESTA","Braço do Norte","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-27.5657701492302,-53.9711308479293,"POSTO DE SAUDE DA SEDE","RUA GARIBALDI","CENTRO","Humaitá","Desempenho muito acima da média"),
                new DAL.Ubs(-10.2488923072812,-36.8835067749013,"CLINICA DE SAUDE DA FAMILIA JURACI RAMOS ROCHA","PRACA JONAS TRINDADE","CENTRO","Cedro de São João","Desempenho acima da média"),
                new DAL.Ubs(-20.3143537044519,-43.5780858993518,"OURO PRETO UNIDADE DE VIGILANCIA SAUDE SAO BARTOLOMEU","RUA CARMO","SAO BARTOLOMEU","Ouro Preto","Desempenho acima da média"),
                new DAL.Ubs(-11.5091228485104,-61.3581061363202,"UNIDADE BASICA DE SAUDE SAO JUDAS TADEU","RUA JOSE MARQUES DE OLIVEIRA","DISTRITO DE RIOZINHO","Cacoal","Desempenho acima da média"),
                new DAL.Ubs(-23.4015119075768,-47.0074725151048,"PSF ARLINDA MARIA DE JESUS","AV ALAOR VEIGAS","JD BOM JESUS","Pirapora do Bom Jesus","Desempenho mediano ou um pouco abaixo da média"),
                new DAL.Ubs(-10.0001978874204,-37.7503323554982,"UNIDADE DA FAMILIA EDVANIA GOMES DOS SANTOS","ZONA RURAL","ZONA RURAL","Poço Redondo","Desempenho muito acima da média"),
                new DAL.Ubs(-7.42972970008829,-38.0418562889088,"PSF DOS GATOS","SITIO DOS GATOS","ZONA RURAL","Nova Olinda","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-26.8803799152366,-49.0455222129808,"ESF ZEBERT KRAUPT","RUA CATARINA AUGUSTA SCHMIDT","FORTALEZA","Blumenau","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-24.0853893756859,-54.2585778236373,"CENTRO DE SAUDE DE GUAIRA","RUA PROFESSOR GALVOSO","CENTRO","Guaíra","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-8.838458061218,-69.259614944456,"CENTRO DE SAUDE INACIO RIBEIRO DA SILVA","RUA VALERIO G DE MAGALHAES","CENTRO","Manoel Urbano","Desempenho mediano ou  um pouco abaixo da média"),
                new DAL.Ubs(-7.95151591300941,-36.218683719634,"UNIDADE DE SAUDE DA FAMILIA DE SANTA TEREZA","RUA JOSE CHAGAS","SANTA TEREZA","Santa Cruz do Capibaribe","Desempenho mediano ou um pouco abaixo da média"),
                new DAL.Ubs(-21.3601362705225,-46.1398959159838,"PSF DO BAIRRO DO ROSARIO","AV TRAJANO VIRGILIO FRANCO","ROSARIO","Areado","Desempenho muito acima da média"),
                new DAL.Ubs(-15.464329719543,-47.6136946678148,"UNIDADE BASICA DE SAUDE 02 QDA04 SUL","QDA 04 MR12","SETOR SUL","Planaltina","Desempenho acima da média"),
                new DAL.Ubs(-23.1955504417413,-52.494757175444,"NIS II TAMBOARA","AVN PARANA","CENTRO","Tamboara","Desempenho mediano ou  um pouco abaixo da média")
            });

            service = new UbsService(repository.Object);
            service.Load("");

            fileResult = new String[2];
            fileResult[0] = $"{filePath}\\resultTestCase1.json";
            fileResult[1] = $"{filePath}\\resultTestCase2.json";
        }

        [TestCase(-26.859207, -49.044939, 0)]
        [TestCase(-26.851866, -49.147408, 1)]
        //[TestCase(-10.020686, -47.507960, 2)] // Falha
        public void TestReturnService(double latitude, double longitude, int indexFileResult)
        {
            var resultToTest = JsonResultTest(fileResult[indexFileResult]);
            var serviceFilter = service.Filter(latitude, longitude);

            Assert.IsTrue(AreEqualCollection(resultToTest, serviceFilter));
        }

        [TestCase(-26.859207, -49.044939, 5)]
        public void TestQtdElements(double latitude, double longitude, int qtdElements)
        {
            var serviceFilter = service.Filter(latitude, longitude);
            Assert.IsTrue(serviceFilter.Count == qtdElements);
        }

        private List<Service.UbsDTO> JsonResultTest(string file)
        {
            var json = File.ReadAllText(file, System.Text.Encoding.GetEncoding("iso-8859-1"));
            return JsonConvert.DeserializeObject<List<Service.UbsDTO>>(json);
        }

        private bool AreEqualCollection(List<Service.UbsDTO> a, List<Service.UbsDTO> b)
        {
            bool ret = true;

            for (int i = 0; i < a.Count; i++)
                if (!a[i].Equals(b[i]))
                    ret = false;

            return ret;
        }

    }
}
